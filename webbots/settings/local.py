from webbots.settings.base import *

# DEBUG
# ------------------------------------------------------------------------------
DEBUG = env.bool('DJANGO_DEBUG', default=True)
INTERNAL_IPS = ['127.0.0.1', ]
ALLOWED_HOSTS = ['*']

TEMPLATES[0]['OPTIONS']['debug'] = DEBUG

# SECRET CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# Note: This key only used for development and testing.
SECRET_KEY = env('DJANGO_SECRET_KEY', default='6w^8(#^r%pop+vwfvm#id=+-5i#@fci2)&y%%i^y9-%2rs-g3o')

# django-debug-toolbar
# ------------------------------------------------------------------------------

DATABASES = {
    'default': env.db('DATABASE_URL', default='mysql://root:password123@localhost:3306/meliorannis'),
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '~ %(asctime)s %(levelname)s %(name)s %(message)s'
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'standard',
        },
    },
    'loggers': {
        'bots': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'django.db.backends': {
            'level': 'INFO',
            # 'level': 'DEBUG',
            'handlers': ['console'],
        }
    },
}

BOTS_MA_BASE_URL = env.str('BOTS_MA_BASE_URL', 'http://localhost:8088')
BOTS_MA_COOKIE_SERVER = env.str('BOTS_MA_COOKIE_SERVER', 'localhost')
