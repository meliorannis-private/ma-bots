# noinspection PyUnresolvedReferences
from webbots.settings.base import *

ALLOWED_HOSTS = ['*']

# SECRET CONFIGURATION
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
# Raises ImproperlyConfigured exception if DJANGO_SECRET_KEY not in os.environ
SECRET_KEY = env('DJANGO_SECRET_KEY')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'standard': {
            'format': '~ %(asctime)s %(levelname)s %(name)s %(message)s'
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'standard',
        },
        'file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': 'bots.log',
            'maxBytes': 1024 * 1024 * 10,  # 10MB
            'backupCount': 10,
            'formatter': 'standard',
        },
    },
    'loggers': {
        'bots': {
            'handlers': ['console', 'file', ],
            'level': 'DEBUG',
            'propagate': True,
        },
        'django.db.backends': {
            'level': 'INFO',
            # 'level': 'DEBUG',
            'handlers': ['console', 'file', ],
        }
    },
}

#
# CUSTOM CONFIGURATION
#

BOTS_MEGA_ARMY_CHANCE = env.float('BOTS_MEGA_ARMY_CHANCE', 0.01)  # 1.0% chance for mega army on a bot
BOTS_MAX_COUNT_BASE = env.int('BOTS_MAX_COUNT_BASE', 1)  # base number of bots active
BOTS_MAX_COUNT_PER_GATE = env.int('BOTS_MAX_COUNT_PER_GATE', 20)  # number of extra bots active per defeated gate
BOTS_MIN_GATE_TO_SPAWN = env.int('BOTS_MIN_GATE_TO_SPAWN', 1)  # gate that must be defeated in order to spawn bots
# min number of seconds after which bot returns CS and retires
BOTS_RETURN_AFTER_SECONDS_MIN = env.int('BOTS_RETURN_AFTER_SECONDS_MIN', 11 * 3600)
# max number of seconds after which bot returns CS and retires
BOTS_RETURN_AFTER_SECONDS_MAX = env.int('BOTS_RETURN_AFTER_SECONDS_MAX', 47 * 3600)
# must match MA settings for $cookieserver var, otherwise fails "security"
BOTS_MA_COOKIE_SERVER = env.str('BOTS_MA_COOKIE_SERVER', 'ma2.meliorannis.com')
# where to contact MA via HTTP
BOTS_MA_BASE_URL = env.str('BOTS_MA_BASE_URL', 'https://ma2.meliorannis.com')
BOTS_SHOW_ADMIN_INFO = env.bool('BOTS_SHOW_ADMIN_INFO', False)
BOTS_DISCORD_WEBHOOK = env.str('BOTS_DISCORD_WEBHOOK', None)
BOTS_DISCORD_PUBLIC_WEBHOOK = env.str('BOTS_DISCORD_PUBLIC_WEBHOOK', None)
BOTS_UI_BACKGROUND = env.str('BOTS_UI_BACKGROUND', None)

# bounties
# min-max number of seconds before returning to post bounty.
BOTS_BOUNTY_POST_BEFORE_MIN_SECONDS = env.int('BOTS_BOUNTY_POST_BEFORE_MIN_SECONDS', 10 * 60)
BOTS_BOUNTY_POST_BEFORE_MAX_SECONDS = env.int('BOTS_BOUNTY_POST_BEFORE_MAX_SECONDS', 180 * 60)
BOTS_BOUNTY_POST_CHANCE = env.float('BOTS_BOUNTY_POST_CHANCE', 0.125)
BOTS_BOUNTY_VALUE = env.int('BOTS_BOUNTY_VALUE', 1)
