# ma-bots

Bots for MA as practice targets.

## Run on MA server

Bots run as a django command (there is also django app, but that's just for some visualization).

Best way is to set up crontab command to execute them. For that, it's recommended to first create an env file with configuration named like `env.speed.sh`:

```
#!/bin/bash

export BOTS_MA_BASE_URL="https://speed.meliorannis.com"
export BOTS_MA_COOKIE_SERVER="speed.meliorannis.com"
export DATABASE_URL="mysql://ma_speed:FILLME@localhost:3306/ma_speed"
export DJANGO_SETTINGS_MODULE="webbots.settings.production"
export DJANGO_SECRET_KEY="FILLME"
export SENTRY_DSN="FILLME"
```

filling correct values to `FILLME` vars. Any other overrides go into this file.

First, load data (armies definitions) using

```
python manage.py loaddata armies spells
```

With that ready, just add a cronjob every 1 minute like:

```
* * * * * . /home/maspeed/mabots/env.speed.sh && . /home/maspeed/.virtualenvs/mabots/bin/activate && cd /home/maspeed/mabots/ && python manage.py tick_bot >> /home/maspeed/mabots/crontab.log 2>&1
```

## Quick cleanup

This query can be performed to quickly remove most bot related data from DB. Mind that attacks are removed as well, leading to issues with CS, don't use in production!

```sql
DELETE FROM bdv WHERE id_user IN (SELECT id FROM users WHERE is_bot = 1);
DELETE FROM jedn WHERE id_user IN (SELECT id FROM users WHERE is_bot = 1);
DELETE FROM users_login WHERE id IN (SELECT id FROM users WHERE is_bot = 1);
DELETE FROM ally WHERE id_user IN (SELECT id FROM users WHERE is_bot = 1);
DELETE FROM aliance WHERE is_bot_alliance = 1;

DELETE FROM users WHERE id IN (SELECT ma_province_id  FROM bots_bot);
DELETE FROM bots_botrebirth WHERE 1=1;
DELETE FROM bots_carpetrequest WHERE 1=1;
DELETE FROM bots_bot WHERE 1=1;
```
