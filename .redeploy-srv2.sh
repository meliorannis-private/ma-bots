#!/bin/bash

echo $(date '+%Y-%m-%d %H:%M:%S') Updating sources...
git -C ~/mabots pull

echo $(date '+%Y-%m-%d %H:%M:%S') Activating env...
. ~/mabots/env.sh

echo $(date '+%Y-%m-%d %H:%M:%S') Activating python virtual...
. ~/.virtualenvs/mabots/bin/activate

echo $(date '+%Y-%m-%d %H:%M:%S') Updating dependencies...
pip install -r ~/mabots/requirements/base.txt

echo $(date '+%Y-%m-%d %H:%M:%S') Migrating db...
python ~/mabots/manage.py migrate bots

echo $(date '+%Y-%m-%d %H:%M:%S') Applying fixtures...
python ~/mabots/manage.py loaddata armies spells

echo $(date '+%Y-%m-%d %H:%M:%S') All done, happy happy
