import doctest

from bots import format


def load_tests(loader, tests, ignore):
    tests.addTests(doctest.DocTestSuite(format))
    return tests
