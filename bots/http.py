import logging
from typing import Optional

import requests
from bs4 import BeautifulSoup
from django.conf import settings

from bots.discord import send_to_private_discord
from bots.format import _thousands
from bots.models import Bot
from bots.random import rand_chance, rand_int

logger = logging.getLogger(__name__)

COOKIE_ACCOUNT_CODE_NAME = 'code'
USER_AGENT_CONNECTION_STRING = 'ma-bots/1.0'


def refresh_prov_load_main(bot: Bot):
    response = requests.get(
        '%s/main.html' % settings.BOTS_MA_BASE_URL,
        headers={
            'User-Agent': USER_AGENT_CONNECTION_STRING,
        },
        cookies={
            COOKIE_ACCOUNT_CODE_NAME: str(bot.ma_province_id),
        }
    )
    response.raise_for_status()


def load_form_prepare_attack(bot: Bot, defender_id: int, combat_id: int) -> Optional[dict]:
    """
    Loads main <form> from a Prepare to attack page.

    If the form is not found for any reason, returns None
    """
    response = requests.get('%s/utok.html' % settings.BOTS_MA_BASE_URL,
                            headers={
                                'User-Agent': USER_AGENT_CONNECTION_STRING,
                            },
                            params={
                                'koho': defender_id,
                                'boj': combat_id,
                            },
                            cookies={
                                COOKIE_ACCOUNT_CODE_NAME: str(bot.ma_province_id),
                            })
    response.raise_for_status()

    soup = BeautifulSoup(response.text, 'html.parser')
    form = soup.find('form', {'name': 'formular'})
    if form is None:
        logger.error('Cannot find a form in HTML for %s targeting %d (battle %d), cannot return CS. HTML %s',
                     bot, defender_id, defender_id, _soup_element_text(soup.find('div', id='central_column_central')))
        return None

    base_inputs = dict({e['name']: e.get('value', '') for e in form.find_all('input', {'name': True})})

    if rand_chance(0.7):
        # 70% for damage
        target_units_at = 1
    else:
        # 10% for each other
        target_units_at = rand_int(2, 5)
    logger.info("Units targeting: %d", target_units_at)

    targeting = dict({e['name']: target_units_at
                      for e
                      in form.find_all('select', {'name': True})
                      if e['name'].startswith('podle_')})

    form_inputs = {**base_inputs, **targeting}

    logger.info("Bot %s loaded his attack screen to %d (battle %d), found keys %s",
                bot, defender_id, combat_id, form_inputs)

    return form_inputs


def load_form_confirm_attack(bot: Bot, form_inputs: dict) -> Optional[dict]:
    """
    Loads main <form> from a Confirm attack page.

    If the form is not found for any reason, returns None
    """
    response = requests.post('%s/boj.html' % settings.BOTS_MA_BASE_URL,
                             headers={
                                 'User-Agent': USER_AGENT_CONNECTION_STRING,
                             },
                             data=form_inputs,
                             cookies={
                                 COOKIE_ACCOUNT_CODE_NAME: str(bot.ma_province_id),
                             })
    response.raise_for_status()

    soup = BeautifulSoup(response.text, 'html.parser')

    form = soup.find('form', {'action': 'bitva.html'})
    if form is None:
        logger.error('Cannot find an attack form in HTML for %s, data %s, HTML %s',
                     bot, form_inputs, _soup_element_text(soup.find('div', id='central_column_wrapper')))
        return None

    form_inputs = dict({e['name']: e.get('value', '') for e in form.find_all('input', {'name': True})})

    logger.info("Bot %s loaded his confirmation attack screen, found keys %s", bot, form_inputs)

    return form_inputs


def commence_attack(bot: Bot, form_inputs: dict, army_power: int) -> None:
    """
    Commence the invasion, comrade!
    """
    response = requests.post('%s/bitva.html' % settings.BOTS_MA_BASE_URL,
                             headers={
                                 'User-Agent': USER_AGENT_CONNECTION_STRING,
                             },
                             data=form_inputs,
                             cookies={
                                 COOKIE_ACCOUNT_CODE_NAME: str(bot.ma_province_id),
                             })
    response.raise_for_status()

    soup = BeautifulSoup(response.text, 'html.parser')

    combat_result = soup.find('span', class_='attack_result_det')
    if combat_result is None:
        logger.error("Bot %s finished his combat, result not found in HTML. Input %s. HTML %s",
                     bot, form_inputs, _soup_element_text(soup.find('div', id='central_column_wrapper')))
        return

    logger.info("Bot %s finished his combat, result %s. Input %s", bot, combat_result.text, form_inputs)
    send_to_private_discord("""\u2694 Bot `%s` returned a CS!
> Army %s
> Spells %s
> Base power: %s
> Final power: %s
> **%s**""" % (bot,
               bot.army,
               bot.combat_spell_settings,
               _thousands(bot.army_power),
               _thousands(army_power),
               combat_result.text))


def _soup_element_text(soup) -> str:
    if soup is None:
        return ''

    text = soup.text
    if text is None:
        return ''

    return text
