import logging
from datetime import timedelta

from django.conf import settings
from django.utils import timezone

from bots.core import initialize_bot, activate_bot, retire_bot, return_possible_cs, perform_cs, \
    check_gate_defeated, calculate_max_bot_count, post_bounties
from bots.models import Bot, CarpetRequest

logger = logging.getLogger(__name__)


def tick() -> None:
    """
    Main cycle, brain of whole bot nation.

    This is what cronjob should invoke.

    It can create new bots, retire existing ones, create job alliance, return CS etc.
    """

    if not check_gate_defeated(settings.BOTS_MIN_GATE_TO_SPAWN):
        logger.debug("First gate has not yet fallen.")
        return

    carpet_performed = _check_carpet()
    if not carpet_performed:
        # regular CS is part of retiring
        _check_bot_retiring()

    _post_possible_bounties()

    _check_activate_new_bot()


def _check_carpet() -> bool:
    """
    Tries an attack in a carpet
    """
    carpet_requests = CarpetRequest.objects.select_related('bot').order_by('created_at').all()

    logger.info("Found %d carpet requests", len(carpet_requests))

    for carpet_request in carpet_requests:
        logger.info("Attempting Carpet %s", carpet_request)
        carpet_request.delete()

        # try to return
        performed = perform_cs(carpet_request.bot, carpet_request.combat_id, carpet_request.player_ma_province_id)
        if performed:
            return True

    return False


def _post_possible_bounties():
    """
    Checks whether there is a bot that is eligible to post a bounty.
    """
    now = timezone.now()
    bot = Bot.objects.filter(active=True, bounty_time_at__lt=now).order_by('bounty_time_at').first()
    if bot is None:
        logger.debug("There is no bot posting rewards to %s", now)
        return

    logger.debug("Posting possible bounties for %s.", bot)
    post_bounties(bot)


def _check_activate_new_bot():
    """
    Checks whether there is enough bots.

    If not, creates and activates new one.
    """
    active_bots = len(Bot.objects.filter(active=True))
    max_bot_count = calculate_max_bot_count()
    if active_bots >= max_bot_count:
        logger.debug('%d/%d bots active, no need to spin up a new one.', active_bots, max_bot_count)
        return

    logger.info("%d/%d bots active, creating a new one.", active_bots, max_bot_count)

    # is there a bot we can reuse?
    expire_threshold = timezone.now() - timedelta(hours=48)
    bot = Bot.objects.filter(active=False, expires_returns_at__lt=expire_threshold).order_by('updated').first()
    if bot is None:
        bot = initialize_bot()

    activate_bot(bot)


def _check_bot_retiring():
    """
    Checks whether there is a bot that is about to retire.

    Note that before retirement, you should return CS.
    """
    now = timezone.now()
    bot = Bot.objects.filter(active=True, expires_returns_at__lt=now).order_by('expires_returns_at').first()
    if bot is None:
        logger.debug("There is no bot retiring prior to %s", now)
        return

    attack_made = return_possible_cs(bot)

    if not attack_made:
        retire_bot(bot)
