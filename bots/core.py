import logging
from datetime import timedelta
from typing import Dict, Optional
from typing import List

from django.conf import settings
from django.db import connection
from django.db.models import Avg, Q, Max
from django.utils import timezone

from bots.discord import send_to_private_discord
from bots.format import _thousands
from bots.http import refresh_prov_load_main, load_form_prepare_attack, load_form_confirm_attack, commence_attack
from bots.models import ArmyUnit, Jedn, Kouzla, Kzl, Obrana, CombatSpellSettings, Rezisty, Aliance, Ally, Utoky, \
    CarpetRequest, Odmeny, MaPosta, MaPostaPrijemci, BotRebirth
from bots.models import Bot, UsersLogin, Users, NocniKlid, UspesnostSouboju, Bdv, MaUsersSetup, MaUsersSpecials, \
    MaUsersSetupMenus, Army, Jednotky
from bots.random import rand_alnum, rand_email, rand_regent_and_sex, rand_class, rand_num, rand_chance, rand_float, \
    rand_int

logger = logging.getLogger(__name__)

CASTLE_BUILDING_ID = 5
BROWSER = 'Mozilla/5.0 (Windows NT 6.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2'
# mini easter egg, who knows what this means :)
ALLIANCE_DESCRIPTION = 'The traditional kings of the jungle command a healthy respect in other climates as well.' \
                       '<br>************************************************************' \
                       '<br>https://melior-annis.fandom.com/cs/wiki/MA_Boti'
BOT_ALLIANCE_ID = -42
BOT_MANA = 2_000_000
RETURN_NOW_OFFSET = timedelta(hours=24)


def initialize_bot() -> Bot:
    """
    Creates a new, blank, bot.

    Sets common province parameters, login/password, but no army.

    Bot is in `active=false` state.
    """
    logger.info("Initializing new bot...")

    bot_name, regent, province, sex = generate_regent_province_name_sex()

    login = password = rand_alnum()
    (class_id, color, spec, class_name) = rand_class()
    logger.info("He is born as %s (%s) from %s, class/color %s%s",
                regent, sex, province, color, spec)

    users_login = UsersLogin(
        undown=login,
        heslo=password,
        ip='A.D.F.G',
        code=rand_num(32),
        code2=rand_num(32),
        kontrola_ip=1,
        kontrola_cook=1,
        browser=BROWSER,
        email=rand_email(),
        reserved_spravce=regent,
        reserved_provi=province,
        ver_text=rand_alnum(8),
        verified=1,
        sledovat=0,
        send_hlidka=0,
    )
    users_login.save()
    user_id = users_login.id

    user = Users(
        id=user_id,
        spravce=regent,
        provincie=province,
        pohlavi=sex,
        # to disable attack prevention
        caszalozeni=timezone.now() - timedelta(hours=48),
        presvedceni='N',
        puvodni_presvedceni='N',
        id_povolani=class_id,
        id_rasa=2,
        protekce_od_casu=int(timezone.now().timestamp()),
        posledni_utok=timezone.now(),
        odehrano_tahu=300,
        protekce=1,
        protekce_ma=1,
        plocha=2000,
        zbyva_tu=360,
        zlato=100_000,
        lide=100_000,
        dane=70,
        max_brana_no=9,
        specializace=spec,
        # as to never get titles
        slava_max=7000,
        titul='RoBoT',
        brana='Ex Aequo',
        barva=color,
        # allow quick login
        verified=1,
        is_bot=1,
    )
    user.save()

    NocniKlid(id_user=user_id).save()
    UspesnostSouboju(id_user=user_id).save()

    Bdv(id_user=user_id, id_bdv=CASTLE_BUILDING_ID, pocet=1).save()

    MaUsersSetup(user_id=user_id).save()
    MaUsersSpecials(id_user=user_id).save()

    bulk = []
    for (item_id, menu_set, item_order) in ((1, 1, 1),
                                            (2, 2, 2),
                                            (3, 2, 1),
                                            (4, 2, 2),
                                            (5, 1, 10),
                                            (6, 1, 5),
                                            (7, 2, 1),
                                            (8, 1, 10),
                                            (9, 1, 4),
                                            (10, 1, 10),
                                            (11, 1, 3),
                                            (12, 2, 1),
                                            (13, 2, 1),
                                            (14, 1, 2),
                                            (15, 1, 12),
                                            (16, 2, 12)):
        bulk.append(
            MaUsersSetupMenus(
                user_id=user_id,
                item_id=item_id,
                menu_set=menu_set,
                item_order=item_order
            )
        )
    MaUsersSetupMenus.objects.bulk_create(bulk)

    # Spells (knows all for his color)
    bulk = []
    for spell in Kouzla.objects.filter(barva__in=[color, '0'], druh='B'):
        bulk.append(
            Kzl(id_kzl=spell.id, id_user=user_id, ucinek=70.0)
        )
    Kzl.objects.bulk_create(bulk)

    Obrana(id_user=user_id).save()

    bot = Bot(
        name=bot_name,
        province=province,
        ma_province_id=user_id,
        ma_login=login,
        ma_password=password,
        color='%s%s' % (color, spec),
        active=False,
    )

    bot.save()

    _add_to_bot_alliance(bot, user)

    return bot


def generate_regent_province_name_sex() -> (str, str, str, str):
    regent_template = '[B] %s'
    province_template = 'Planet %s'
    bot_name_template_suffix = '%s [%03d]'
    regent_template_suffix = '[B] %s [%03d]'
    province_template_suffix = 'Planet %s [%03d]'

    # first try to find a random unique name 10 times. Yeah, this could be done in reverse (first fetch names, compare
    # to what we have in names list. But, given that this will happen ~50 times per age, it's not that much of a load
    for i in range(20):
        (base_name, sex) = rand_regent_and_sex()
        bot_name = base_name
        regent = regent_template % base_name
        province = province_template % base_name

        # new
        if Users.objects.filter(Q(spravce=regent) | Q(provincie=province)).count() == 0:
            logger.info("Found unique name for province: %s/%s/%s", regent, province, sex)
            return bot_name, regent, province, sex

        logger.info("Name %s/%s is already taken, trying another one", regent, province)

    # give up after 10 tries. Stick to a single one and try suffixes
    (base_name, sex) = rand_regent_and_sex()

    bot_name = base_name
    regent = regent_template % base_name
    province = province_template % base_name

    while Users.objects.filter(Q(spravce=regent) | Q(provincie=province)).count() > 0:
        logger.info("Name %s/%s is already taken, trying another one", regent, province)

        suffix = rand_int(1000)
        bot_name = bot_name_template_suffix % (base_name, suffix)
        regent = regent_template_suffix % (base_name, suffix)
        province = province_template_suffix % (base_name, suffix)

    logger.info("Found unique name for province: %s/%s/%s", regent, province, sex)
    return bot_name, regent, province, sex


def activate_bot(bot: Bot) -> None:
    """
    Activates bot, calculates random power, area, sets when it should go inactive/return CS.
    """
    logger.info("%s: Activating bot", bot)

    if bot.active:
        logger.error("%s: Bot is already active!", bot)
        return

    average_area = _find_average_area()
    min_area = 3_500
    bot_area = rand_int(min_area, average_area)

    logger.debug("%s: Area range: [%d; %d], selected %d", bot, min_area, average_area, bot_area)

    average_ot = _find_average_ot()
    army_power = _army_power(bot_area)
    magic_level = _bot_magic_level(average_ot, bot_area / average_area)
    spell_tier, spell_xp = _spell_tier_xp_by_magic_tier(magic_level)
    spell_resist = _spell_resist_by_ot(magic_level)

    logger.debug("%s: area: %d, army power: %d, magic level %d, spell tier %d, spell xp %.3f, spell resist %.3f",
                 bot, bot_area, army_power, magic_level, spell_tier, spell_xp, spell_resist)

    user_id = bot.ma_province_id
    user = Users.objects.get(id=user_id)
    user.mana = BOT_MANA
    user.zlato = bot_area * 75
    user.lide = bot_area * 50
    user.plocha = bot_area
    user.zbyva_tu = 360
    user.slava = int(bot_area ** 1.6 / 10_000)
    user.protekce_ma = 120
    user.odehrano_tahu = 1_000  # bot alliance support is hard-coded in MA
    user.protekce = 1

    user.save()

    _refresh_buildings(bot, bot_area)

    # Select army
    army = Army.objects.filter(color=bot.color).order_by('?').first()
    combat_spell_settings = CombatSpellSettings.objects.filter(army=army, tier=spell_tier).first()
    logger.info("%s: Selected army: %s", bot, army)
    logger.info("%s: Selected spell settings: %s", bot, combat_spell_settings)

    # MEGA_ARMY
    mega_army = False
    if rand_chance(settings.BOTS_MEGA_ARMY_CHANCE):
        logger.info("%s: Bot has MEGA ARMY", bot)
        mega_army = True
        army = Army.objects.filter(color='XX').order_by('?').first()

        if army is None:
            logger.error("%s: Unable to find any MEGA ARMY", bot)

    _update_army_to(bot, army, army_power)
    _update_defense_spells_to(bot, combat_spell_settings)

    # update spell xp
    Kzl.objects.filter(id_user=user_id).update(ucinek=spell_xp * 100.0)

    _update_resistance(bot, user.odehrano_tahu, spell_resist)

    # refresh provi in MA, recalculates full power
    refresh_prov_load_main(bot)

    # update bot flags
    bot.army = army
    bot.combat_spell_settings = combat_spell_settings
    bot.army_power = army_power
    bot.active = True
    bot.expires_returns_at = timezone.now() + timedelta(
        seconds=rand_int(settings.BOTS_RETURN_AFTER_SECONDS_MIN, settings.BOTS_RETURN_AFTER_SECONDS_MAX))
    bot.bounty_time_at = bot.expires_returns_at - timedelta(
        seconds=rand_int(settings.BOTS_BOUNTY_POST_BEFORE_MIN_SECONDS, settings.BOTS_BOUNTY_POST_BEFORE_MAX_SECONDS))
    bot.last_activated = timezone.now()

    bot.save()
    logger.info("%s: Returns / expires at %s, bounty time %s", bot, bot.expires_returns_at, bot.bounty_time_at)

    # remove from prot
    user.protekce = 0
    user.save(update_fields=['protekce'])
    logger.info("%s: Updated, removing safety 1TU prot", bot)

    send_to_private_discord("""\U0001F916 Bot `%s` was activated!
> Army: %s%s
> Army Power: %s
> Magic: %s
> Spell XP: %.2f%%
> Bounty time: %s
> Returns at: %s""" % (bot, army, ' @here' if mega_army else '', _thousands(army_power), combat_spell_settings,
                       spell_xp * 100.0, bot.bounty_time_at, bot.expires_returns_at))

    BotRebirth(
        bot=bot,
        average_area=average_area,
        min_area=min_area,
        bot_area=bot_area,
        average_ot=average_ot,
        army_power=army_power,
        magic_level=magic_level,
        spell_tier=spell_tier,
        spell_xp=spell_xp,
        spell_resist=spell_resist,
        army=army,
        mega_army=mega_army,
        combat_spell_settings=combat_spell_settings,
        expires_returns_at=bot.expires_returns_at,
        bounty_time_at=bot.bounty_time_at,
    ).save()


def retire_bot(bot: Bot) -> None:
    user = Users.objects.filter(id=bot.ma_province_id).first()
    if user is not None:
        _reset_buildings(bot)

        user.plocha = 2_000
        user.protekce = 1
        user.save(update_fields=['plocha', 'protekce'])
        _update_army_to(bot, None, 0)
        refresh_prov_load_main(bot)

        logger.info("%s: Reset bot to 2.000 hAr and 1 TU VProt", bot)

    bot.active = False
    bot.save()
    logger.info("%s: Sent to protection and deactivated him.", bot)


def post_bounties(bot: Bot, force_bounty: bool = False) -> None:
    if bot.bounty_time_at is None and not force_bounty:
        logger.error("%s: Invoked safety, bot is not supposed to post bounties, nor was a force-bounty flag used.", bot)
        return

    # safety first as not to double-post
    bot.bounty_time_at = None
    bot.save()

    # all primary attacks against bot
    attacks = Utoky.objects.filter(
        Q(kdy__gt=(timezone.now() - timedelta(hours=48)).timestamp())
        &
        Q(kdy__gt=bot.return_cs_safety_flag),
        koho=bot.ma_province_id,
        je_to_prvoutok=1,
        protiutok_pouzito=0,
    ).values('kdo').all()

    for attack in attacks:
        player_id = attack['kdo']
        if force_bounty or rand_chance(settings.BOTS_BOUNTY_POST_CHANCE):
            player_power = Users.objects.filter(id=player_id).values('power').first()['power']
            bounty_amount = settings.BOTS_BOUNTY_VALUE * player_power * rand_float(2.0, 4.0)

            logger.info("%s: Will post a bounty of %d to %d", bot, bounty_amount, player_id)

            _post_bounty(bot, player_id, bounty_amount)

            _send_ma_post(bot, _human_cs_against(player_id), """Zdravím Tě, člověče,<br><br>
moji špehové mě informovali, že máme společného nepřítele číslo %d. Mám v plánu na něj zaútočit kolem %s.<br><br>
Vypsal jsem na něj odměnu %s, doufám, že mi pomůžeš. V opačném případě budu muset vyhlásit válku celému lidstvu.<br><br>
-- %s""" % (
                player_id,
                timezone.localtime(bot.expires_returns_at).strftime('%H:%M'),
                _thousands(bounty_amount),
                bot.name
            ))


def _human_cs_against(player_id: int) -> List[int]:
    result = []
    with connection.cursor() as cursor:
        cursor.execute("""SELECT DISTINCT utoky.koho
FROM utoky
JOIN users ON users.id = utoky.koho
WHERE utoky.kdo = %s
  AND utoky.protiutok_pouzito = 0
  AND utoky.je_to_prvoutok = 1
  AND utoky.kdy > %s
  AND users.is_bot = 0""", [player_id, (timezone.now() - timedelta(hours=48)).timestamp()])
        for row in cursor.fetchall():
            result.append(row[0])

    return result


def _send_ma_post(bot: Bot, player_ids: List[int], text: str) -> None:
    if not player_ids:
        return

    mail = MaPosta(
        odesilatel_id=bot.ma_province_id,
        # ugly hack over MA's time fields that are TZ incompatible. Just make it a string, the field as CharField and
        # hope it will work out
        kdy=timezone.localtime(timezone.now()).strftime('%Y-%m-%d %H:%M:%S'),
        predmet="NULL",
        telo=text,
        priorita=0,
        odesilatel_smazat=0,
    )
    mail.save()

    for player_id in player_ids:
        MaPostaPrijemci(
            posta_id=mail.id,
            prijemce_id=player_id,
            precteno=0,
        ).save()

    logger.debug("Mail sent to %s" % player_ids)


def _post_bounty(bot: Bot, player_id: int, bounty_amount: int) -> None:
    bot_user = Users.objects.get(id=bot.ma_province_id)

    # is there a bounty?
    alignment = bot_user.presvedceni
    current_bounty = Odmeny.objects.filter(id_koho=player_id, pro_presvedceni=alignment).values('odmena').first()

    if current_bounty is not None:
        current_bounty_value = current_bounty['odmena']
        new_bounty_value = current_bounty_value + bounty_amount

        logger.debug("%s: Increased bounty to %d from %d to %d", bot, player_id, current_bounty_value, new_bounty_value)

        Odmeny.objects.filter(id_koho=player_id, pro_presvedceni=alignment).update(odmena=new_bounty_value)

        send_to_private_discord("""\U0001F4B0 Bot `%s` has increased bounty!
        > Target ID: %d
        > From: %s
        > To: %s
        > By: %s""" % (bot,
                       player_id,
                       _thousands(current_bounty_value),
                       _thousands(new_bounty_value),
                       _thousands(bounty_amount)))

        return

    Odmeny(id_user=bot_user.id,
           vypisovatel=bot_user.spravce,
           id_koho=player_id,
           odmena=bounty_amount,
           identifikator=rand_int(65000),
           pro_presvedceni=alignment).save()

    send_to_private_discord("""\U0001F4B0 Bot `%s` has posted bounty!
    > Target ID: %d
    > Amount: %s""" % (bot, player_id, _thousands(bounty_amount)))


def return_possible_cs(bot: Bot) -> bool:
    """
    Tries to find a CS and if there is one, return it.

    Returns True iff an attack was attempted (even if the code then fails), or False if there are no more CS to try
    and province should retire to inactive.
    """
    attack = Utoky.objects.filter(
        Q(kdy__gt=(timezone.now() - timedelta(hours=48)).timestamp())
        &
        Q(kdy__gt=bot.return_cs_safety_flag),
        koho=bot.ma_province_id,
        je_to_prvoutok=1,
        protiutok_pouzito=0,
    ).values('kdo', 'kdy', 'id_boje').order_by('kdy').first()

    if attack is None:
        logger.debug("%s: There is no CS.", bot)
        return False

    player_id = attack['kdo']
    combat_id = attack['id_boje']
    logger.info("%s: There is a CS for bot: target %d, since %d, combat_id %d",
                bot, player_id, attack['kdy'], combat_id)

    bot.return_cs_safety_flag = attack['kdy']
    bot.save(update_fields=['return_cs_safety_flag'])

    perform_cs(bot, combat_id, player_id)

    if not _is_player_in_nigh_calm(player_id):
        request_carpet(bot, attack)

    return True


def perform_cs(bot: Bot, combat_id: int, player_ma_province_id: int) -> bool:
    # before attack, reset mana, give protection (good also in case of exceptions, provi stays in prot 5 TU) and add
    # huge amount of population (case be an issue with some armies otherwise)

    population = bot.army_power * 100
    gold = bot.army_power * 10
    logger.debug("%s: Updated province population to %d, VProt to 5, mana to %d, gold to %d.",
                 bot, population, BOT_MANA, gold)
    Users.objects.filter(id=bot.ma_province_id).update(zlato=gold,
                                                       mana=BOT_MANA,
                                                       protekce=5,
                                                       lide=population)

    result = _perform_cs_internal(bot, combat_id, player_ma_province_id)

    # after CS, give him prot again, just to keep it clean and tidy
    Users.objects.filter(id=bot.ma_province_id).update(protekce=1)

    # some counters
    bot.returned_cs_count += 1
    bot.last_cs_returned = timezone.now()
    bot.save(update_fields=['returned_cs_count', 'last_cs_returned'])

    return result


def _perform_cs_internal(bot: Bot, combat_id: int, player_ma_province_id: int) -> bool:
    safety = Utoky.objects.filter(
        kdy__gt=(timezone.now() - timedelta(hours=48)).timestamp(),
        koho=bot.ma_province_id,
        je_to_prvoutok=1,
        protiutok_pouzito=0,
        id_boje=combat_id,
        kdo=player_ma_province_id,
    ).count()

    if safety == 0:
        logger.error("%s: Combat %d (%d => %d) not found, skipping returning of CS.",
                     bot, combat_id, bot.ma_province_id, player_ma_province_id)
        return False

    army_power = _bot_cs_power(bot, player_ma_province_id)
    logger.debug("%s: Final army power for CS: %d", bot, army_power)

    _update_army_to(bot, bot.army, army_power)

    prepare_form_inputs = load_form_prepare_attack(bot, player_ma_province_id, combat_id)
    if prepare_form_inputs is None:
        logger.error('%s: Problem returning CS (player %d / combat %d).', bot, player_ma_province_id, combat_id)
        return False

    prepare_form_inputs['kouzlo_1'] = bot.combat_spell_settings.ma_spell_1_id or 0
    prepare_form_inputs['kouzlo_2'] = bot.combat_spell_settings.ma_spell_2_id or 0
    prepare_form_inputs['kouzlo_3'] = bot.combat_spell_settings.ma_spell_3_id or 0
    prepare_form_inputs['kouzlo_4'] = bot.combat_spell_settings.ma_spell_4_id or 0
    prepare_form_inputs['kouzlo_1_koho'] = 1
    prepare_form_inputs['kouzlo_2_koho'] = 1
    prepare_form_inputs['kouzlo_3_koho'] = 1
    prepare_form_inputs['kouzlo_4_koho'] = 1

    confirm_form_inputs = load_form_confirm_attack(bot, prepare_form_inputs)
    if confirm_form_inputs is None:
        logger.error('%s: Problem returning CS (player %d / combat %d).', bot, player_ma_province_id, combat_id)
        return False

    confirm_form_inputs['message'] = bot.army.label
    commence_attack(bot, confirm_form_inputs, army_power)

    return True


def _bot_cs_power(bot: Bot, player_ma_province_id: int) -> int:
    player_data = Users.objects.filter(id=player_ma_province_id).values('power', 'plocha').first()
    target_player_power = player_data['power']
    target_player_area = player_data['plocha']
    bot_area = Users.objects.filter(id=bot.ma_province_id).values('plocha').first()['plocha']

    logger.info("%s: Calculating CS power, target %d, target power %d, target area %d, bot area %d",
                bot, player_ma_province_id, target_player_power, target_player_area, bot_area)

    if rand_chance(0.07):
        logger.debug("%s: Base power in 0.8625-1.0125 range", bot)
        base_power = int(target_player_power * rand_float(0.9, 1.05))

    elif rand_chance(0.27):
        logger.debug("%s: Base power in 0.75-0.8625 range", bot)
        base_power = int(target_player_power * rand_float(0.79, 0.9))

    else:
        logger.debug("%s: Base power in 0.45-0.75 range", bot)
        base_power = int(target_player_power * rand_float(0.49, 0.79))

    number_of_active_bot_cs_on_player = Utoky.objects.filter(
        kdy__gt=(timezone.now() - timedelta(hours=48)).timestamp(),
        kdo=player_ma_province_id,
        je_to_prvoutok=1,
        protiutok_pouzito=0,
        koho__in=Users.objects.filter(is_bot=1).values('id')
    ).count()

    courage_coef = 1.0

    if number_of_active_bot_cs_on_player > 2:
        courage_coef = (0.8 + (number_of_active_bot_cs_on_player * 0.1))

    top_player_coef = 1.0 + (int(target_player_area / 10000) * 0.05)

    multiplier = 1.0

    if bot_area > target_player_area:
        multiplier = bot_area / target_player_area

    final_power = int(base_power * top_player_coef * multiplier * courage_coef)
    logger.info("%s: Base power %d, area multiplier %.2f, final power %d", bot, base_power, multiplier, final_power)

    return final_power


def request_carpet(bot: Bot, attack: dict) -> None:
    """
    Informs all other bots that this one has returned a CS. They should wake up and return ASAP as well.
    """

    other_bots = Bot.objects.exclude(id=bot.id).all()  # type: List[Bot]
    bot_ma_province_ids = {other_bot.ma_province_id: other_bot for other_bot in other_bots}

    # find all attacks against other bots by this particular player
    player_id = attack['kdo']
    counter_strikes = Utoky.objects.filter(
        kdy__gt=(timezone.now() - timedelta(hours=48)).timestamp(),
        kdo=player_id,
        je_to_prvoutok=1,
        protiutok_pouzito=0,
        koho__in=bot_ma_province_ids.keys(),
    ).values('koho', 'id_boje').all()

    if not counter_strikes:
        logger.debug("%s: There are no CS for target %d", bot, player_id)
        return

    bulk = []
    for counter_strike in counter_strikes:
        bulk.append(
            CarpetRequest(
                bot=bot_ma_province_ids[counter_strike['koho']],
                combat_id=counter_strike['id_boje'],
                player_ma_province_id=player_id,
            )
        )
    CarpetRequest.objects.bulk_create(bulk)

    logger.info("%s: Found attacks %s that share CS against %d, marked them as carpet CS.",
                bot, counter_strikes, player_id)


def _is_player_in_nigh_calm(user_id: int) -> bool:
    with connection.cursor() as cursor:
        cursor.execute("""SELECT
            IF(
                `start_time` <= `end_time`,
                (CURTIME() <= `end_time` AND CURTIME() >= `start_time`),
                (CURTIME() >= `start_time` OR CURTIME() <= `end_time`)
            )
        FROM `nocni_klid`
        WHERE `id_user` = %s""", [user_id])

        row = cursor.fetchone()
        if row is not None and row[0]:
            logger.info("Player %d is having night calm (NK).", user_id)
            return True

    logger.info("Player %d is NOT having night calm (NK).", user_id)
    return False


def check_gate_defeated(gate_number: int) -> bool:
    return _highest_gate_defeated() >= gate_number


def calculate_max_bot_count() -> int:
    return settings.BOTS_MAX_COUNT_BASE + _highest_gate_defeated() * settings.BOTS_MAX_COUNT_PER_GATE


def _highest_gate_defeated() -> int:
    return Users.objects.filter(is_bot=0, id__gte=1000).aggregate(Max('brana_no'))['brana_no__max']


def _add_to_bot_alliance(bot: Bot, user: Users) -> None:
    if user.id_aliance is not None and user.id_aliance > 0:
        logger.debug("%s: Bot is already in alliance", bot)
        # already in alliance
        return

    alliance = _get_or_create_bot_alliance(bot)

    user.id_aliance = alliance.id_aliance
    user.save(update_fields=['id_aliance'])

    Ally(
        id_user=bot.ma_province_id,
        id_aliance=alliance.id_aliance,
    ).save()

    alliance_user_key = ',%d,' % bot.ma_province_id
    if alliance_user_key in alliance.clenove:
        # alliance creator
        return

    alliance.clenove += alliance_user_key
    alliance.save()


def _get_or_create_bot_alliance(bot: Bot) -> Aliance:
    """
    Tries to lookup bot alliance.

    If not found, new is created.

    If found, as an easter egg, it's message is changed to a random DBZ quote ;)
    """
    alliance = Aliance.objects.filter(is_bot_alliance=1).first()

    if alliance is not None:
        logger.debug("%s: Alliance i%s exists.", bot, alliance)
        return alliance

    alliance = Aliance(
        id_aliance=BOT_ALLIANCE_ID,
        presvedceni='N',
        nazev_aliance='[B] Z-Fighters',
        popis_aliance=ALLIANCE_DESCRIPTION,
        vudce=bot.ma_province_id,
        clenove=',%d,' % bot.ma_province_id,
        is_bot_alliance=1,
        http='melior-annis.fandom.com/cs/wiki/MA_Boti',
    )
    alliance.save()

    logger.info("%s: Created new alliance %s", bot, alliance)
    return alliance


def _bot_magic_level(ot: int, area_ratio: float) -> int:
    # tier 16
    if ot > 7400 and area_ratio > 0.75:
        return 16

    # tier 15
    if ot > 7400 and area_ratio > 0.50:
        return 15

    # tier 14
    if ot > 7400 and area_ratio > 0.35:
        return 14

    # tier 13
    if ot > 6900 and area_ratio > 0.75:
        return 13

    # tier 12
    if ot > 6400 and area_ratio > 0.70:
        return 12

    # tier 11
    if ot > 5900 and area_ratio > 0.65:
        return 11

    # tier 10
    if ot > 5400 and area_ratio > 0.65:
        return 10

    # tier 9
    if ot > 4900 and area_ratio > 0.55:
        return 9

    # tier 8
    if ot > 4400 and area_ratio > 0.60:
        return 8

    # tier 7
    if ot > 3900 and area_ratio > 0.50:
        return 7

    # tier 6
    if ot > 3400 and area_ratio > 0.55:
        return 6

    # tier 5
    if ot > 2900 and area_ratio > 0.75:
        return 5

    # tier 4
    if ot > 2900 and area_ratio > 0.45:
        return 4

    # tier 3
    if ot > 2400 and area_ratio > 0.50:
        return 3

    # tier 2
    if ot > 1800 and area_ratio > 0.40:
        return 2

    # tier 1
    return 1


def _spell_tier_xp_by_magic_tier(magic_tier: int) -> (int, float):
    # tier 16
    if magic_tier == 16:
        return 3, rand_float(0.01, 0.99)

    # tier 15
    if magic_tier == 15:
        return 2, rand_float(0.01, 0.99)

    # tier 14
    if magic_tier == 14:
        return 1, rand_float(0.01, 0.99)

    # tier 13
    if magic_tier == 13:
        return 2, rand_float(0.60, 0.90)

    # tier 12
    if magic_tier == 12:
        return 3, rand_float(0.20, 0.75)

    # tier 11
    if magic_tier == 11:
        return 2, rand_float(0.20, 0.75)

    # tier 10
    if magic_tier == 10:
        return 3, rand_float(0.15, 0.60)

    # tier 9
    if magic_tier == 9:
        return 3, rand_float(0.10, 0.45)

    # tier 8
    if magic_tier == 8:
        return 2, rand_float(0.15, 0.60)

    # tier 7
    if magic_tier == 7:
        return 3, rand_float(0.05, 0.25)

    # tier 6
    if magic_tier == 6:
        return 2, rand_float(0.10, 0.45)

    # tier 5
    if magic_tier == 5:
        return 1, rand_float(0.60, 0.90)

    # tier 4
    if magic_tier == 4:
        return 1, rand_float(0.15, 0.65)

    # tier 3
    if magic_tier == 3:
        return 2, rand_float(0.05, 0.25)

    # tier 2
    if magic_tier == 2:
        return 1, rand_float(0.10, 0.50)

    # tier 1
    return 1, rand_float(0.05, 0.25)


def _spell_resist_by_ot(magic_tier: int) -> float:
    if magic_tier >= 13:
        return 0.80

    if magic_tier == 12:
        return 0.79

    if magic_tier == 11:
        return 0.78

    if magic_tier == 10:
        return 0.77

    if magic_tier == 9:
        return 0.76

    if magic_tier == 8:
        return 0.75

    if magic_tier == 7:
        return 0.73

    if magic_tier == 6:
        return 0.71

    if magic_tier == 5:
        return 0.69

    if magic_tier == 4:
        return 0.67

    if magic_tier == 3:
        return 0.65

    if magic_tier == 2:
        return 0.60

    return 0.50


def _army_power(area: int) -> int:
    """
    Army power based on area.
    """
    if area < 4_500:
        return int(rand_float(2.5, 5.0) * area)

    if area < 7_000:
        return int(rand_float(3.0, 8.0) * area)

    if area < 10_000:
        return int(rand_float(3.5, 9.0) * area)

    if area < 14_000:
        return int(rand_float(4.0, 10.0) * area)

    if area < 20_000:
        return int(rand_float(4.5, 12.0) * area)

    return int(rand_float(5.0, 15.0) * area)


def _find_average_area() -> int:
    """
    Finds average area over TOP 5 players
    """
    users = list(user.plocha for user in Users.objects.filter(is_bot=False, id__gte=1_000).order_by('-plocha')[:5])
    average_area = int(sum(users) / len(users))
    logger.debug("Average area of TOP5: %d", average_area)
    return average_area


def _find_average_ot() -> int:
    """
    Finds average TU played over players with OT > 500
    """
    average_ot = int(Users.objects.filter(odehrano_tahu__gt=500, is_bot=False, id__gte=1_000)
                     .aggregate(Avg('odehrano_tahu'))['odehrano_tahu__avg'])
    logger.debug("Average OT of players with 500+ OT: %d", average_ot)
    return average_ot


def _update_resistance(bot: Bot, user_ot: int, resist: float) -> None:
    Rezisty.objects.filter(id_user=bot.ma_province_id).delete()

    bulk = []
    for c in ('B', 'C', 'F', 'M', 'S', 'Z'):
        bulk.append(
            Rezisty(id_user=bot.ma_province_id,
                    barva=c,
                    zvyseny_r=int(resist * 100),
                    do_tahu=user_ot + 1_000)
        )

    Rezisty.objects.bulk_create(bulk)


def _refresh_buildings(bot: Bot, area: int) -> None:
    # demolish everything except castle
    Bdv.objects.filter(id_user=bot.ma_province_id).exclude(id_bdv=CASTLE_BUILDING_ID).delete()

    bulk = []
    # buildings
    for (building_id, count) in (
            (2, int(area * 0.1)),  # farms
            (6, int(area * 0.09)),  # mag towers
            (25, int(area * 0.045)),  # mag labs
            (1, int(area * 0.002)),  # towns
            (3, _number_of_forts(area)),  # forts
            (10, 9_999),  # spies
    ):
        if count > 0:
            logger.debug("%s: Built %dx%d", bot, count, building_id)
            bulk.append(
                Bdv(id_user=bot.ma_province_id, id_bdv=building_id, pocet=count)
            )

    Bdv.objects.bulk_create(bulk)


def _reset_buildings(bot: Bot) -> None:
    # demolish everything except castle
    Bdv.objects.filter(id_user=bot.ma_province_id).exclude(id_bdv=CASTLE_BUILDING_ID).delete()

    # build/keep 200 farms and 5 towns
    Bdv.objects.bulk_create([
        Bdv(id_user=bot.ma_province_id, id_bdv=1, pocet=5),
        Bdv(id_user=bot.ma_province_id, id_bdv=2, pocet=200),
    ])
    logger.debug("%s: Reset bot to 1 castle, 5 towns and 200 farms", bot)


def _number_of_forts(area: int) -> int:
    """
    1 extra fort per 5000 hAR of area after reaching 10000 hAR

    >>> _number_of_forts(0)
    0
    >>> _number_of_forts(14999)
    0
    >>> _number_of_forts(15000)
    1
    >>> _number_of_forts(21000)
    2
    """
    return max(0, int((area - 10000) / 5000))


def _update_army_to(bot: Bot, army: Optional[Army], army_power: int) -> None:
    logger.info("%s: Updating army of to %s", bot, army)

    Jedn.objects.filter(id_user=bot.ma_province_id).delete()

    if army is None:
        return

    units_setup = army.armyunit_set.all()  # type: List[ArmyUnit]
    ma_units = Jednotky.objects.filter(id__in=[unit.ma_unit_id for unit in units_setup])

    ma_units_by_id = {unit.id: unit for unit in ma_units}  # type: Dict[int, Jednotky]

    bulk = []
    for unit_setup in units_setup:
        ma_unit = ma_units_by_id[unit_setup.ma_unit_id]

        if unit_setup.count_absolute is not None:
            count = unit_setup.count_absolute
        else:
            count = int(army_power
                        * unit_setup.count_percent
                        / unit_setup.experience
                        / ma_unit.power)

        if count < 1:
            count = 1

        bulk.append(Jedn(
            id_user=bot.ma_province_id,
            id_jednotky=ma_unit.id,
            pocet_jedn=count,
            zkusenost=unit_setup.experience * 100,
        ))

    Jedn.objects.bulk_create(bulk)


def _update_defense_spells_to(bot: Bot, combat_spell_settings: CombatSpellSettings) -> None:
    defense = Obrana.objects.get(id_user=bot.ma_province_id)
    defense.id_kzl_1 = 0
    defense.id_kzl_2 = 0
    defense.id_kzl_3 = 0
    defense.id_kzl_4 = 0

    if combat_spell_settings is not None:
        defense.id_kzl_1 = combat_spell_settings.ma_spell_1_id
        defense.id_kzl_2 = combat_spell_settings.ma_spell_2_id
        defense.id_kzl_3 = combat_spell_settings.ma_spell_3_id
        defense.id_kzl_4 = combat_spell_settings.ma_spell_4_id

    defense.save()
