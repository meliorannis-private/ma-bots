# Generated by Django 3.1.3 on 2020-11-26 17:40

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('bots', '0003_auto_20201119_0946'),
    ]

    operations = [
        migrations.CreateModel(
            name='CarpetRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('player_ma_province_id', models.PositiveIntegerField()),
                ('combat_id', models.PositiveIntegerField()),
                ('bot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bots.bot')),
            ],
        ),
    ]
