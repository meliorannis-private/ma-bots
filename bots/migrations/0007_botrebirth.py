# Generated by Django 3.1.3 on 2021-01-18 14:44

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('bots', '0006_battlehistory_battlehistoryunits_maposta_mapostaprijemci_odmeny'),
    ]

    operations = [
        migrations.CreateModel(
            name='BotRebirth',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('average_area', models.IntegerField()),
                ('min_area', models.IntegerField()),
                ('bot_area', models.IntegerField()),
                ('average_ot', models.IntegerField()),
                ('army_power', models.IntegerField()),
                ('magic_level', models.IntegerField()),
                ('spell_tier', models.IntegerField()),
                ('spell_xp', models.FloatField()),
                ('spell_resist', models.FloatField()),
                ('mega_army', models.BooleanField()),
                ('expires_returns_at', models.DateTimeField()),
                ('bounty_time_at', models.DateTimeField()),
                ('army', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL,
                                           to='bots.army')),
                ('bot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bots.bot')),
                ('combat_spell_settings',
                 models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL,
                                   to='bots.combatspellsettings')),
            ],
        ),
    ]
