# Generated by Django 3.1.3 on 2020-12-17 15:49

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('bots', '0005_bot_bountytime_combatspellsettings_label'),
    ]

    operations = [
        migrations.CreateModel(
            name='BattleHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('battle_timestamp', models.IntegerField()),
                ('type', models.CharField(max_length=6)),
                ('winner', models.CharField(max_length=8)),
                ('id_attacker', models.IntegerField()),
                ('id_defender', models.IntegerField(blank=True, null=True)),
                ('color_attacker', models.CharField(max_length=1)),
                ('color_defender', models.CharField(blank=True, max_length=1, null=True)),
                ('specialisation_attacker', models.CharField(max_length=1)),
                ('specialisation_defender', models.CharField(blank=True, max_length=1, null=True)),
                ('name_attacker', models.CharField(max_length=256)),
                ('name_defender', models.CharField(max_length=256)),
                ('province_attacker', models.CharField(max_length=256)),
                ('province_defender', models.CharField(blank=True, max_length=256, null=True)),
                ('losses_attacker', models.FloatField()),
                ('losses_defender', models.FloatField()),
                ('body_gzipped', models.TextField()),
                ('power_before_attacker', models.IntegerField()),
                ('power_before_defender', models.IntegerField()),
                ('power_after_attacker', models.IntegerField()),
                ('power_after_defender', models.IntegerField()),
                ('target_quality', models.FloatField(blank=True, null=True)),
                ('battle_quality', models.CharField(blank=True, max_length=50, null=True)),
                ('area_taken', models.IntegerField(blank=True, null=True)),
                ('glory_attacker', models.IntegerField(blank=True, null=True)),
                ('glory_defender', models.IntegerField(blank=True, null=True)),
                ('gate_number', models.IntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'battle_history',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='BattleHistoryUnits',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('id_battle_history', models.IntegerField()),
                ('is_attacker', models.IntegerField()),
                ('support_id', models.IntegerField(blank=True, null=True)),
                ('name', models.CharField(max_length=64)),
                ('color', models.CharField(max_length=1)),
                ('count', models.IntegerField()),
                ('count_original', models.IntegerField()),
                ('ini', models.IntegerField()),
                ('attack_type', models.CharField(max_length=1)),
                ('movement_type', models.CharField(max_length=1)),
                ('speed', models.IntegerField()),
                ('attack_target', models.CharField(max_length=16)),
                ('xp', models.FloatField(blank=True, null=True)),
                ('power', models.FloatField(blank=True, null=True)),
                ('power_orig', models.FloatField(blank=True, null=True)),
            ],
            options={
                'db_table': 'battle_history_units',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='MaPosta',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('odesilatel_id', models.PositiveIntegerField()),
                ('pro_alianci_id', models.IntegerField(blank=True, null=True)),
                ('reply_to_id', models.PositiveBigIntegerField(blank=True, null=True)),
                ('kdy', models.CharField(max_length=32)),
                ('predmet', models.CharField(blank=True, max_length=255, null=True)),
                ('telo', models.TextField(blank=True, null=True)),
                ('priorita', models.IntegerField()),
                ('odesilatel_smazat', models.IntegerField()),
            ],
            options={
                'db_table': 'ma_posta',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='MaPostaPrijemci',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('posta_id', models.PositiveBigIntegerField()),
                ('prijemce_id', models.PositiveIntegerField()),
                ('precteno', models.IntegerField()),
            ],
            options={
                'db_table': 'ma_posta_prijemci',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Odmeny',
            fields=[
                ('id_user', models.PositiveIntegerField()),
                ('vypisovatel', models.CharField(max_length=50)),
                ('id_koho', models.PositiveIntegerField(primary_key=True, serialize=False)),
                ('odmena', models.PositiveIntegerField()),
                ('identifikator', models.PositiveSmallIntegerField()),
                ('pro_presvedceni', models.CharField(max_length=1)),
            ],
            options={
                'db_table': 'odmeny',
                'managed': False,
            },
        ),
    ]
