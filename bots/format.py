import re
from typing import Optional


def _thousands(value: int) -> str:
    """
    >>> _thousands(100)
    '100'

    >>> _thousands(1234)
    '1 234'

    >>> _thousands(123456789)
    '123 456 789'
    """
    return f'{value:,}'.replace(',', ' ')


def sanitize_regent(regent: Optional[str]) -> str:
    if regent is None:
        return '---'
    return re.sub(r'^<.*?> *([^<>]+)</\w+>$', r'\1', regent)
