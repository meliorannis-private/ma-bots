import logging

from django.core.management.base import BaseCommand

from bots.tick import tick

logger = logging.getLogger(__name__)


class Command(BaseCommand):

    def __init__(self, stdout=None, stderr=None, no_color=False, force_color=False):
        super().__init__(stdout, stderr, no_color, force_color)

    help = "Ticking Gnomes are at th ready to produce more bots."

    def handle(self, *args, **options):
        logger.info("Tick commencing...")

        try:
            tick()
        except Exception as e:
            logger.exception("Got an exception from tick: %s", e)
