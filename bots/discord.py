import requests
from django.conf import settings


def send_to_private_discord(message: str) -> None:
    webhook = settings.BOTS_DISCORD_WEBHOOK
    if webhook is None:
        return

    requests.post(webhook, json={'content': message})


def send_to_public_discord(payload: dict) -> None:
    webhook = settings.BOTS_DISCORD_PUBLIC_WEBHOOK
    if webhook is None:
        return

    requests.post(webhook, json=payload)
