import gzip
from typing import Optional, Set

from django import template
from django.utils.safestring import mark_safe

from bots.format import _thousands
from bots.models import BattleHistory

register = template.Library()


@register.filter
def nf_thousands(value: int) -> str:
    return mark_safe(_thousands(value).replace(' ', '&nbsp;'))


@register.filter
def nf_pm(value: int, negate: bool = False) -> str:
    if value is None:
        return mark_safe('&mdash;')

    if negate:
        value = -value

    if value > 0:
        return mark_safe('+%s' % _thousands(value).replace(' ', '&nbsp;'))

    return mark_safe(_thousands(value).replace(' ', '&nbsp;').replace('-', '&minus;'))


@register.filter
def mult(a: int, b: int) -> int:
    return a * b


@register.filter
def battle_result_class(battle: BattleHistory, bot_ids: Set[int]) -> str:
    if battle.winner == 'tie':
        return 'text-secondary'

    is_attacker = battle.id_attacker in bot_ids

    if battle.winner == 'attacker' and is_attacker:
        return 'text-success'
    if battle.winner == 'defender' and not is_attacker:
        return 'text-success'

    return 'text-danger'


@register.filter
def is_bot(player_id: int, bot_ids: Set[int]) -> bool:
    return player_id in bot_ids


@register.filter
def is_bot_attacker(battle_history: BattleHistory, bot_ids: Set[int]) -> bool:
    return battle_history.id_attacker in bot_ids


@register.filter
def un_gzip(gzipped) -> str:
    return mark_safe(gzip.decompress(gzipped).decode('utf-8'))


@register.filter
def sanitize_regent(regent: Optional[str]) -> str:
    from bots.format import sanitize_regent as _sanitize_regent
    return _sanitize_regent(regent)


@register.filter
def floor(value: Optional[float]) -> Optional[int]:
    if value is None:
        return None

    return int(value)
