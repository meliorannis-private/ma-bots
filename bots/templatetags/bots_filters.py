from datetime import datetime
from typing import Optional

from django import template
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.timezone import make_aware

register = template.Library()


@register.filter
def time_dm_hm(value: Optional[datetime]) -> str:
    """
    Returns date/time in long format 'd.m. HH:mm'.

    Uses non-retarded format (sorry US).
    """
    if not value:
        return mark_safe('&mdash;')

    return '{d.day}.{d.month}. {d.hour:02}:{d.minute:02}'.format(
        d=timezone.get_current_timezone().normalize(value))


@register.filter
def get_dict_item(dictionary, key):
    return dictionary.get(key)


@register.filter
def timestamp_to_time(timestamp):
    return make_aware(datetime.fromtimestamp(int(timestamp)))
