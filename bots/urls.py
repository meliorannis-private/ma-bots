from django.urls import path

from bots import views

app_name = 'bots'
urlpatterns = [
    path('', views.bot_list, name='bot-list'),
    path('battle-history-csv/', views.battle_history_csv, name='battle_history_csv'),
    path('battle-history-xlsx/', views.battle_history_xlsx, name='battle_history_xlsx'),
    path('battle-history-xlsx-pvp/', views.battle_history_xlsx_pvp, name='battle_history_xlsx_pvp'),
    path('tick/', views.execute_tick, name='tick'),
    path('init-bot/', views.init, name='init'),
    path('activate/<int:bot_id>/', views.activate, name='activate'),
    path('return-cs/<int:bot_id>/', views.return_cs, name='return_cs'),
    path('retire/<int:bot_id>/', views.retire, name='retire'),
    path('post-bounties/<int:bot_id>/', views.bounties, name='post_bounties'),
    path('battle-history/', views.battle_history_list, name='battle_history_list'),
    path('battle-history/<int:battle_history_id>/', views.battle_history_detail, name='battle_history_detail'),
]
