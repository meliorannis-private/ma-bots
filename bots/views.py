import csv
import io
from datetime import timedelta
from typing import List

import xlsxwriter
from django.conf import settings
from django.db import connection
from django.db.models import Q
from django.http import HttpResponse
from django.http import HttpResponseForbidden
from django.shortcuts import redirect, get_object_or_404, render
from django.utils import timezone
from django.views.decorators.http import require_POST

from bots.core import initialize_bot, activate_bot, retire_bot, return_possible_cs, calculate_max_bot_count, \
    post_bounties
from bots.format import sanitize_regent
from bots.models import Bot, BattleHistory, BattleHistoryUnits
from bots.tick import tick


# noinspection DuplicatedCode
def bot_list(request):
    bots = Bot.objects \
        .select_related('army', 'combat_spell_settings') \
        .order_by('-active', 'expires_returns_at', 'id').all()

    with connection.cursor() as cursor:
        cursor.execute("""SELECT utoky.kdo,
       utoky.koho,
       utoky.kdy,
       utoky.ztraty_kdo,
       utoky.ztraty_koho,
       utoky.zabrano_har,
       utoky.sila_utocnika,
       utoky.sila_obrance,
       utoky.protiutok_pouzito,
       users.spravce,
       users.provincie,
       aliance.nazev_aliance
FROM utoky
LEFT JOIN users ON users.id = utoky.kdo
LEFT JOIN aliance ON users.id_aliance = aliance.id_aliance
WHERE utoky.koho IN (SELECT ma_province_id FROM bots_bot)
  AND utoky.je_to_prvoutok = 1
  AND kdy > %s
ORDER BY utoky.kdy DESC""", [(timezone.now() - timedelta(hours=48)).timestamp()])

        columns = [col[0] for col in cursor.description]
        defenses = [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]

    with connection.cursor() as cursor:
        cursor.execute("""SELECT utoky.kdo,
       utoky.koho,
       utoky.kdy,
       utoky.ztraty_kdo,
       utoky.ztraty_koho,
       utoky.zabrano_har,
       utoky.sila_utocnika,
       utoky.sila_obrance,
       utoky.protiutok_pouzito,
       users.spravce,
       users.provincie,
       aliance.nazev_aliance
FROM utoky
LEFT JOIN users ON users.id = utoky.koho
LEFT JOIN aliance ON users.id_aliance = aliance.id_aliance
WHERE utoky.kdo IN (SELECT ma_province_id FROM bots_bot)
  AND kdy > %s
ORDER BY utoky.kdy DESC""", [(timezone.now() - timedelta(hours=48)).timestamp()])

        columns = [col[0] for col in cursor.description]
        attacks = [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]

    combats_by_bot_ma_id = {}

    for combat in defenses:
        key = combat['koho']
        combat['is_defense'] = True
        combat['ztraty_kdo'] = int(combat['ztraty_kdo'] / 100)
        combat['ztraty_koho'] = int(combat['ztraty_koho'] / 100)
        combat['spravce'] = sanitize_regent(combat['spravce'])
        if key not in combats_by_bot_ma_id:
            combats_by_bot_ma_id[key] = []

        combats_by_bot_ma_id[key].append(combat)

    for combat in attacks:
        key = combat['kdo']
        combat['is_defense'] = False
        combat['ztraty_kdo'] = int(combat['ztraty_kdo'] / 100)
        combat['ztraty_koho'] = int(combat['ztraty_koho'] / 100)
        combat['spravce'] = sanitize_regent(combat['spravce'])
        if key not in combats_by_bot_ma_id:
            combats_by_bot_ma_id[key] = []

        combats_by_bot_ma_id[key].append(combat)

    for combat in combats_by_bot_ma_id.values():
        combat.sort(key=lambda c: -c['kdy'])

    return render(request, 'bots/bot_list.html', {
        'bots': bots,
        'combats_by_bot_ma_id': combats_by_bot_ma_id,
        'show_admin_info': settings.BOTS_SHOW_ADMIN_INFO,
        'ui_background': settings.BOTS_UI_BACKGROUND,
        'max_bot_count': calculate_max_bot_count(),
        'active_bot_count': len(Bot.objects.filter(active=True)),
    })


def battle_history_list(request):
    all_bots = list(Bot.objects.all())
    bot_ma_ids = list(bot.ma_province_id for bot in all_bots)

    battles = BattleHistory.objects.filter(Q(id_attacker__in=bot_ma_ids) | Q(id_defender__in=bot_ma_ids)) \
        .order_by('-battle_timestamp') \
        .all()  # type: List[BattleHistory]

    return render(request, 'bots/battle_history_list.html', {
        'battles': battles,
        'bot_ids': set(bot_ma_ids),
        # 'bots': all_bots.
    })


def battle_history_detail(request, battle_history_id):
    all_bots = list(Bot.objects.all())
    bot_ma_ids = list(bot.ma_province_id for bot in all_bots)

    battle = get_object_or_404(BattleHistory.objects
                               .filter(Q(id_attacker__in=bot_ma_ids) | Q(id_defender__in=bot_ma_ids),
                                       id=battle_history_id))  # type: BattleHistory

    units = list(BattleHistoryUnits.objects.filter(id_battle_history=battle_history_id).all())
    attacker_units = list(unit for unit in units if unit.is_attacker == 1)  # type: List[BattleHistoryUnits]
    defender_units = list(unit for unit in units if unit.is_attacker != 1)  # type: List[BattleHistoryUnits]
    attacker_units.sort(key=lambda u: [u.support_id if u.support_id is not None else 0, - u.count_original * u.power])
    defender_units.sort(key=lambda u: [u.support_id if u.support_id is not None else 0, - u.count_original * u.power])

    return render(request, 'bots/battle_history_detail.html', {
        'battle': battle,
        'attacker_units': attacker_units,
        'defender_units': defender_units,
    })


@require_POST
def execute_tick(request):
    if not settings.BOTS_SHOW_ADMIN_INFO:
        return HttpResponseForbidden('Forbidden')

    tick()

    return redirect('bots:bot-list')


@require_POST
def init(request):
    if not settings.BOTS_SHOW_ADMIN_INFO:
        return HttpResponseForbidden('Forbidden')

    initialize_bot()

    return redirect('bots:bot-list')


@require_POST
def activate(request, bot_id: int):
    if not settings.BOTS_SHOW_ADMIN_INFO:
        return HttpResponseForbidden('Forbidden')

    bot = get_object_or_404(Bot, id=bot_id)
    activate_bot(bot)

    return redirect('bots:bot-list')


@require_POST
def return_cs(request, bot_id: int):
    if not settings.BOTS_SHOW_ADMIN_INFO:
        return HttpResponseForbidden('Forbidden')

    bot = get_object_or_404(Bot, id=bot_id)
    return_possible_cs(bot)

    return redirect('bots:bot-list')


@require_POST
def retire(request, bot_id: int):
    if not settings.BOTS_SHOW_ADMIN_INFO:
        return HttpResponseForbidden('Forbidden')

    bot = get_object_or_404(Bot, id=bot_id)
    retire_bot(bot)

    return redirect('bots:bot-list')


@require_POST
def bounties(request, bot_id: int):
    if not settings.BOTS_SHOW_ADMIN_INFO:
        return HttpResponseForbidden('Forbidden')

    bot = get_object_or_404(Bot, id=bot_id)
    post_bounties(bot, force_bounty=True)

    return redirect('bots:bot-list')


def battle_history_csv(request):
    # Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/plain; charset=utf-8')
    response['Content-Disposition'] = 'inline'

    writer = csv.writer(response)
    writer.writerow(
        ['utok id', 'utok spravce', 'obrana id', 'obrana spravce', 'prvo', 'ztraty utok', 'ztraty obrana',
         'sila utok', 'sila obrana', 'kdy', 'kdy', 'typ', 'zisk hAr', 'utok slava', 'obrana slava'])

    for row in _battle_history_data():
        writer.writerow(row)

    return response


def battle_history_xlsx(request):
    # Create an in-memory output file for the new workbook.
    output = io.BytesIO()

    # Even though the final file will be in memory the module uses temp
    # files during assembly for efficiency. To avoid this on servers that
    # don't allow temp files, for example the Google APP Engine, set the
    # 'in_memory' Workbook() constructor option as shown in the docs.
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet()

    # Get some data to write to the spreadsheet.
    data = [['utok id', 'utok spravce', 'obrana id', 'obrana spravce', 'prvo', 'ztraty utok', 'ztraty obrana',
             'sila utok', 'sila obrana', 'kdy', 'kdy', 'typ', 'zisk hAr', 'utok slava',
             'obrana slava']] + _battle_history_data()

    for row_num, row in enumerate(data):
        for col_num, col in enumerate(row):
            worksheet.write(row_num, col_num, col)

    # Close the workbook before sending the data.
    workbook.close()

    # Rewind the buffer.
    output.seek(0)

    # Set up the Http response.
    filename = 'bots-%s.xlsx' % (timezone.now().strftime('%Y-%m-%d %H-%M-%S'))
    response = HttpResponse(
        output,
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    )
    response['Content-Disposition'] = 'attachment; filename=%s' % filename

    return response


def _battle_history_data():
    result = []

    with connection.cursor() as cursor:
        cursor.execute("""SELECT attacker.id, attacker.spravce,
           defender.id, defender.spravce,
           utoky.je_to_prvoutok,
           ROUND((1 - sila_utocnika_end / sila_utocnika)  * 100, 2),
           ROUND((1 - sila_obrance_end / sila_obrance)  * 100, 2),
           sila_utocnika,
           sila_obrance,
           kdy,
           FROM_UNIXTIME(kdy, '%Y-%m-%d %H:%i:%s'),
           battle_history.type,
           battle_history.area_taken,
           battle_history.glory_attacker,
           battle_history.glory_defender
    FROM utoky
    JOIN users AS attacker ON utoky.kdo = attacker.id
    JOIN users AS defender ON utoky.koho = defender.id
    LEFT JOIN battle_history ON utoky.battle_history_id = battle_history.id
    WHERE (attacker.is_bot = 1 OR defender.is_bot = 1)
    ORDER BY kdy""")

        while True:
            row = cursor.fetchone()
            if row is None:
                break

            row = list(row)

            row[1] = sanitize_regent(row[1])
            row[3] = sanitize_regent(row[3])
            result.append(row)

    return result


def battle_history_xlsx_pvp(request):
    # Create an in-memory output file for the new workbook.
    output = io.BytesIO()

    # Even though the final file will be in memory the module uses temp
    # files during assembly for efficiency. To avoid this on servers that
    # don't allow temp files, for example the Google APP Engine, set the
    # 'in_memory' Workbook() constructor option as shown in the docs.
    workbook = xlsxwriter.Workbook(output)
    worksheet = workbook.add_worksheet()

    # Get some data to write to the spreadsheet.
    data = [['utok id', 'utok spravce', 'obrana id', 'obrana spravce', 'prvo', 'ztraty utok', 'ztraty obrana',
             'sila utok', 'sila obrana', 'kdy', 'kdy', 'typ', 'zisk hAr', 'utok slava',
             'obrana slava']] + _battle_history_data_pvp()

    for row_num, row in enumerate(data):
        for col_num, col in enumerate(row):
            worksheet.write(row_num, col_num, col)

    # Close the workbook before sending the data.
    workbook.close()

    # Rewind the buffer.
    output.seek(0)

    # Set up the Http response.
    filename = 'bots-%s.xlsx' % (timezone.now().strftime('%Y-%m-%d %H-%M-%S'))
    response = HttpResponse(
        output,
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    )
    response['Content-Disposition'] = 'attachment; filename=%s' % filename

    return response


def _battle_history_data_pvp():
    result = []

    with connection.cursor() as cursor:
        cursor.execute("""SELECT attacker.id, attacker.spravce,
           defender.id, defender.spravce,
           utoky.je_to_prvoutok,
           ROUND((1 - sila_utocnika_end / sila_utocnika)  * 100, 2),
           ROUND((1 - sila_obrance_end / sila_obrance)  * 100, 2),
           sila_utocnika,
           sila_obrance,
           kdy,
           FROM_UNIXTIME(kdy, '%Y-%m-%d %H:%i:%s'),
           battle_history.type,
           battle_history.area_taken,
           battle_history.glory_attacker,
           battle_history.glory_defender
    FROM utoky
    JOIN users AS attacker ON utoky.kdo = attacker.id
    JOIN users AS defender ON utoky.koho = defender.id
    LEFT JOIN battle_history ON utoky.battle_history_id = battle_history.id
    WHERE (attacker.is_bot = 0 AND defender.is_bot = 0)
    ORDER BY kdy""")

        while True:
            row = cursor.fetchone()
            if row is None:
                break

            row = list(row)

            row[1] = sanitize_regent(row[1])
            row[3] = sanitize_regent(row[3])
            result.append(row)

    return result
